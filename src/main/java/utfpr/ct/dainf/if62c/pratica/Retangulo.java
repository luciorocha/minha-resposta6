package utfpr.ct.dainf.if62c.pratica;

/**
 * Programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Retangulo implements FiguraComLados {
    private double base;
    private double altura;

    public Retangulo() {
        base = altura = 0;
    }

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
 
    public double getArea() {
        return base * altura;
    }
    
    public double getPerimetro() {
        return (base + altura) * 2;
    }
    
    public double getLadoMenor(){
        return base;
    }
    public double getLadoMaior(){
        return altura;
    }
}
