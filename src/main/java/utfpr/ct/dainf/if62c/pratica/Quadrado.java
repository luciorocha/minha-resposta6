package utfpr.ct.dainf.if62c.pratica;

/**
 * Programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Quadrado extends Retangulo implements FiguraComLados {

    public Quadrado() {
        super();
    }

    public Quadrado(double lado) {
        super(lado, lado);
    }
    
    public double getLadoMenor(){
        return super.getLadoMenor();
    }
    public double getLadoMaior(){
        return super.getLadoMaior();
    }
}
