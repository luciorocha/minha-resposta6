package utfpr.ct.dainf.if62c.pratica;

/**
 * Programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public interface Figura {
    //public String getNome();
    public double getPerimetro();
    public double getArea();
}
