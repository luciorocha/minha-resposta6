/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author root
 */
public class Circulo extends Elipse {
    private double r;
    
    public Circulo(double r){
        super();
        this.r=r;
    }
    
    @Override
    public double getPerimetro(){
        return 2*Math.PI*this.r;
    }
    
    @Override
    public double getArea(){        
        return Math.PI*Math.pow(this.r,2);
    }
}
