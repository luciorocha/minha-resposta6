/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
      
import utfpr.ct.dainf.if62c.pratica.*;
public class Pratica43 {
    public static void main(String [] args){
        
        Figura f1 = new Circulo(10);
        Figura f2 = new Elipse(10, 20);
        Figura f3 = new Quadrado(10);
        Figura f4 = new Retangulo(10,20);
        Figura f5 = new TrianguloEquilatero(10);
        
        System.out.println("Circulo (area): " + f1.getArea());
        System.out.println("Circulo (perimetro): " + f1.getPerimetro());
        System.out.println("Elipse (area): " + f2.getArea());
        System.out.println("Elipse (perimetro): " + f2.getPerimetro());        
        System.out.println("Quadrado (area): " + f3.getArea());
        System.out.println("Quadrado (perimetro): " + f3.getPerimetro());
        System.out.println("Retangulo (area): " + f4.getArea());
        System.out.println("Retangulo (perimetro): " + f4.getPerimetro());
        System.out.println("TrianguloEquilatero (area): " + f5.getArea());
        System.out.println("TrianguloEquilatero (perimetro): " + f5.getPerimetro());
        
    }
}
